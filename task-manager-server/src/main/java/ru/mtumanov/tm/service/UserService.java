package ru.mtumanov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.repository.IProjectRepository;
import ru.mtumanov.tm.api.repository.ITaskRepository;
import ru.mtumanov.tm.api.repository.IUserRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.api.service.IPropertyService;
import ru.mtumanov.tm.api.service.IUserService;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.entity.EntityEmptyException;
import ru.mtumanov.tm.exception.field.EmailEmptyException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.exception.field.LoginEmptyException;
import ru.mtumanov.tm.exception.field.PasswordEmptyException;
import ru.mtumanov.tm.exception.user.ExistLoginException;
import ru.mtumanov.tm.model.User;
import ru.mtumanov.tm.util.HashUtil;

import java.util.Collection;
import java.util.List;

public class UserService implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    final IConnectionService connectionService;

    public UserService(@NotNull final IConnectionService connectionService, @NotNull final IPropertyService propertyService) {
        this.connectionService = connectionService;
        this.propertyService = propertyService;
    }

    @Override
    @NotNull
    public User create(@NotNull final String login, @NotNull final String password) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        if (isLoginExist(login))
            throw new ExistLoginException();
        if (password.isEmpty())
            throw new PasswordEmptyException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            try {
                @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
                userRepository.add(user);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
        return user;
    }

    @Override
    @NotNull
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final String email
    ) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        if (isLoginExist(login))
            throw new ExistLoginException();
        if (password.isEmpty())
            throw new PasswordEmptyException();
        if (email.isEmpty())
            throw new EmailEmptyException();
        @NotNull final User user = create(login, password);
        user.setEmail(email);
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            try {
                @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
                userRepository.update(user);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
        return user;
    }

    @Override
    @NotNull
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final Role role
    ) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        if (isLoginExist(login))
            throw new ExistLoginException();
        if (password.isEmpty())
            throw new PasswordEmptyException();
        @NotNull final User user = create(login, password);
        user.setRole(role);
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            try {
                @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
                userRepository.update(user);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
        return user;
    }

    @Override
    @NotNull
    public User findByLogin(@NotNull final String login) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findByLogin(login);
        }
    }

    @Override
    @NotNull
    public User findByEmail(@NotNull final String email) throws AbstractException {
        if (email.isEmpty())
            throw new EmailEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findByEmail(email);
        }
    }

    @Override
    @NotNull
    public User findById(@NotNull final String id) throws AbstractException {
        if (id.isEmpty())
            throw new LoginEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findById(id);
        }
    }

    @Override
    @NotNull
    public User removeByLogin(@NotNull final String login) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            try {
                @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
                @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
                @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
                @NotNull final User user = userRepository.findByLogin(login);
                taskRepository.clear(user.getId());
                projectRepository.clear(user.getId());
                userRepository.removeById(user.getId());
                sqlSession.commit();
                return user;
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
    }

    @Override
    @NotNull
    public User removeByEmail(@NotNull final String email) throws AbstractException {
        if (email.isEmpty())
            throw new EmailEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            try {
                @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
                @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
                @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
                @NotNull final User user = userRepository.findByEmail(email);
                taskRepository.clear(user.getId());
                projectRepository.clear(user.getId());
                userRepository.update(user);
                sqlSession.commit();
                return user;
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
    }

    @Override
    @NotNull
    public User setPassword(@NotNull final String id, @NotNull final String password) throws AbstractException {
        if (id.isEmpty())
            throw new IdEmptyException();
        if (password.isEmpty())
            throw new PasswordEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            try {
                @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
                @NotNull final User user = userRepository.findById(id);
                user.setPasswordHash(HashUtil.salt(propertyService, password));
                userRepository.update(user);
                sqlSession.commit();
                return user;
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
    }

    @Override
    @NotNull
    public User userUpdate(
            @NotNull final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws AbstractException {
        if (id.isEmpty())
            throw new IdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            try {
                @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
                @NotNull final User user = userRepository.findById(id);
                user.setFirstName(firstName);
                user.setLastName(lastName);
                user.setMiddleName(middleName);
                userRepository.update(user);
                sqlSession.commit();
                return user;
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
    }

    @Override
    public boolean isLoginExist(@NotNull final String login) {
        if (login.isEmpty())
            return false;
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.isLoginExist(login);
        }
    }

    @Override
    public boolean isEmailExist(@NotNull final String email) {
        if (email.isEmpty())
            return false;
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.isLoginExist(email);
        }
    }

    @Override
    public void lockUserByLogin(@NotNull final String login) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            try {
                @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
                @NotNull final User user = userRepository.findByLogin(login);
                user.setLocked(true);
                userRepository.update(user);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
    }

    @Override
    public void unlockUserByLogin(@NotNull final String login) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            try {
                @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
                @NotNull final User user = userRepository.findByLogin(login);
                user.setLocked(false);
                userRepository.update(user);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
    }

    @Override
    @NotNull
    public List<User> findAll() {
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findAll();
        }
    }

    @Override
    @NotNull
    public Collection<User> set(@NotNull Collection<User> models) throws AbstractException {
        if (models == null)
            throw new EntityEmptyException(User.class.getName());
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            try {
                @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
                userRepository.clear();
                for (@NotNull final User model : models)
                    userRepository.add(model);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
        return models;
    }

}
