package ru.mtumanov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.repository.IProjectRepository;
import ru.mtumanov.tm.api.repository.ITaskRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.api.service.IProjectTaskService;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.entity.ProjectNotFoundException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.model.Task;

import java.sql.SQLException;
import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull IConnectionService connectionService;

    public ProjectTaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Override
    public void bindTaskToProject(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) throws AbstractException, SQLException {
        if (projectId.isEmpty())
            throw new IdEmptyException();
        if (taskId.isEmpty())
            throw new IdEmptyException();
        try (@NotNull SqlSession sqlSession = connectionService.getConnection()) {
            try {
                @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
                @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
                if (!projectRepository.existById(userId, projectId))
                    throw new ProjectNotFoundException();
                @NotNull final Task task = taskRepository.findOneById(userId, taskId);

                task.setProjectId(projectId);
                taskRepository.update(task);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
    }

    @Override
    public void removeProjectById(@NotNull final String userId, @NotNull final String projectId)
            throws AbstractException, SQLException {
        if (projectId.isEmpty())
            throw new IdEmptyException();
        try (@NotNull SqlSession sqlSession = connectionService.getConnection()) {
            try {
                @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
                @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
                if (!projectRepository.existById(userId, projectId))
                    throw new ProjectNotFoundException();
                @NotNull final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
                for (final Task task : tasks)
                    taskRepository.removeById(userId, task.getId());
                projectRepository.removeById(userId, projectId);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
    }

    @Override
    public void unbindTaskFromProject(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) throws AbstractException, SQLException {
        if (projectId.isEmpty())
            throw new IdEmptyException();
        if (taskId.isEmpty())
            throw new IdEmptyException();
        try (@NotNull SqlSession sqlSession = connectionService.getConnection()) {
            try {
                @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
                @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
                if (!projectRepository.existById(userId, projectId))
                    throw new ProjectNotFoundException();
                @NotNull final Task task = taskRepository.findOneById(userId, taskId);
                task.setProjectId(null);
                taskRepository.update(task);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
    }

}
