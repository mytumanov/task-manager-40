package ru.mtumanov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.model.User;

import java.util.List;

public interface IUserRepository {

    @Insert("INSERT INTO tm_user (id, login, password_hash, email, first_name, last_name, mid_name, role, locked)" +
            " VALUES (#{id}, #{login}, #{passwordHash}, #{email}, #{firstName}, #{lastName}, #{middleName}, #{role}, #{locked})")
    void add(@NotNull User model);

    @NotNull
    @Select("SELECT * FROM tm_user WHERE login = #{login}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "mid_name")
    })
    User findByLogin(@Param("login") @NotNull String login);

    @NotNull
    @Select("SELECT * FROM tm_user WHERE email = #{email}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "mid_name")
    })
    User findByEmail(@Param("email") @NotNull String email);

    @NotNull
    @Select("SELECT * FROM tm_user WHERE id = #{id}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "mid_name")
    })
    User findById(@Param("id") @NotNull String id);

    @NotNull
    @Select("SELECT * FROM tm_user")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "mid_name")
    })
    List<User> findAll();

    @Select("SELECT EXISTS(SELECT 1 FROM tm_user WHERE login = #{login})")
    boolean isLoginExist(@Param("login") @NotNull String login);

    @Select("SELECT EXISTS(SELECT 1 FROM tm_user WHERE email = #{email})")
    boolean isEmailExist(@Param("email") @NotNull String email);

    @NotNull
    @Update("UPDATE tm_user SET login = #{login}," +
            "password_hash = #{passwordHash}," +
            "email = #{email}," +
            "first_name = #{firstName}," +
            "last_name = #{lastName}," +
            "mid_name = #{middleName}," +
            "role = #{role}," +
            "locked = #{locked} " +
            "WHERE id = #{id}")
    void update(@NotNull User model);

    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    void removeById(@Param("id") @NotNull String id);

    @Delete("DELETE FROM tm_user")
    void clear();

}
