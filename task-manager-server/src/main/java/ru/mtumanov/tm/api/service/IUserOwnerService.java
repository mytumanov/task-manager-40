package ru.mtumanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.AbstractUserOwnedModel;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public interface IUserOwnerService<M extends AbstractUserOwnedModel> extends IService<M> {

    @NotNull
    List<M> findAll(@NotNull String userId) throws AbstractException, SQLException;

    @NotNull
    M add(@NotNull String userId, @NotNull M model) throws AbstractException, SQLException;

    @NotNull
    List<M> findAll(@NotNull String userId, @Nullable Comparator<M> comparator) throws AbstractException, SQLException;

    @NotNull
    M findOneById(@NotNull String userId, @NotNull String id) throws AbstractException, SQLException;

    @NotNull
    void remove(@NotNull String userId, @NotNull M model) throws AbstractException, SQLException;

    @NotNull
    void removeById(@NotNull String userId, @NotNull String id) throws AbstractException, SQLException;

    void clear(@NotNull String userId) throws AbstractException, SQLException;

    int getSize(@NotNull String userId) throws AbstractException, SQLException;

    boolean existById(@NotNull String userId, @NotNull String id) throws AbstractException, SQLException;

}
