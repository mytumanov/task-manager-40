package ru.mtumanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.AbstractModel;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public interface IService<M extends AbstractModel> {

    @NotNull
    List<M> findAll() throws SQLException;

    @NotNull
    Collection<M> set(@NotNull Collection<M> models) throws SQLException, AbstractException;

}
