package ru.mtumanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.Session;

import java.util.List;

public interface ISessionService {

    @NotNull
    Session add(@Nullable String userId, @Nullable Session model) throws AbstractException;

    @NotNull
    List<Session> findAll();

    @NotNull
    List<Session> findAll(@Nullable String userId) throws AbstractException;

    @NotNull
    Session findOneById(@Nullable String userId, @Nullable String id) throws AbstractException;

    @NotNull
    void remove(@Nullable Session model) throws AbstractException;

    @NotNull
    void removeById(@Nullable String userId, @Nullable String id) throws AbstractException;

}
