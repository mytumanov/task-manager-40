package ru.mtumanov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import ru.mtumanov.tm.model.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskRepository {

    @NotNull
    @Select("SELECT id, created, name, description, status, user_id, project_id FROM tm_task WHERE user_id = #{userId} AND project_id = #{projectId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<Task> findAllByProjectId(@Param("userId") @NotNull String userId, @Param("projectId") @NotNull String projectId);

    @Insert("INSERT INTO tm_task (id, created, name, description, status, user_id, project_id)" +
            " VALUES (#{id}, #{created}, #{name}, #{description}, #{status}, #{userId}, #{projectId})")
    void add(@NotNull Task model);

    @Insert("INSERT INTO tm_task (id, created, name, description, status, user_id, project_id)" +
            " VALUES (#{id}, #{created}, #{name}, #{description}, #{status}, #{userId}, #{projectId})")
    void addAll(@NotNull Collection<Task> model);

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId}")
    void clear(@Param("userId") @NotNull String userId);

    @Delete("DELETE FROM tm_task")
    void clearAll();

    @Select("SELECT EXISTS(SELECT 1 FROM tm_task WHERE userId = #{userId} AND id = #{id})")
    boolean existById(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @NotNull
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<Task> findAllUserId(@NotNull String userId);

    @NotNull
    @Select("SELECT * FROM tm_task")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<Task> findAll();

    @NotNull
    @SelectProvider(type = SqlProviderAdapter.class, method = "select")
    List<Task> findAllComporator(@NotNull SelectStatementProvider selectStatement);

    @NotNull
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} AND id = #{id}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    Task findOneById(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Select("SELECT COUNT(1) FROM tm_task WHERE user_id = #{userId}")
    int getSize(@Param("userId") @NotNull String userId);

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId} AND id = #{id}")
    void removeById(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Update("UPDATE tm_task SET created = #{created}," +
            "name = #{name}," +
            "description = #{description}," +
            "status = #{status}," +
            "user_id = #{userId}," +
            "project_id = #{projectId}" +
            " WHERE id = #{id}")
    void update(@NotNull Task model);

}
