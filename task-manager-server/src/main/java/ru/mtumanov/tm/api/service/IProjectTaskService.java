package ru.mtumanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.exception.AbstractException;

import java.sql.SQLException;

public interface IProjectTaskService {

    void bindTaskToProject(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) throws AbstractException, SQLException;

    void removeProjectById(@NotNull String userId, @NotNull String projectId) throws AbstractException, SQLException;

    void unbindTaskFromProject(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) throws AbstractException, SQLException;

}
