package ru.mtumanov.tm.dynamicmodel;

import org.jetbrains.annotations.NotNull;
import org.mybatis.dynamic.sql.SqlColumn;

import java.util.Date;

public final class ProjectDynamicSql {

    @NotNull
    public static final ProjectTable projectTable = new ProjectTable();

    @NotNull
    public static final SqlColumn<String> id = projectTable.id;

    @NotNull
    public static final SqlColumn<Date> created = projectTable.created;

    @NotNull
    public static final SqlColumn<String> description = projectTable.description;

    @NotNull
    public static final SqlColumn<String> status = projectTable.status;

    @NotNull
    public static final SqlColumn<String> projectUserId = projectTable.userId;

    @NotNull
    public static final SqlColumn<String> name = projectTable.name;

}
