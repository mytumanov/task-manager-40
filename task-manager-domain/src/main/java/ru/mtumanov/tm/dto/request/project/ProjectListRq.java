package ru.mtumanov.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.AbstractUserRq;
import ru.mtumanov.tm.enumerated.ProjectSort;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectListRq extends AbstractUserRq {

    @Nullable
    private ProjectSort sort;

    public ProjectListRq(@Nullable final String token, @Nullable final ProjectSort sort) {
        super(token);
        this.sort = sort;
    }

}