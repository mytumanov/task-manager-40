package ru.mtumanov.tm.exception.system;

import org.jetbrains.annotations.NotNull;

public final class CommandNotSupportedException extends AbstractSytemException {

    public CommandNotSupportedException() {
        super("ERROR! Command not supported!");
    }

    public CommandNotSupportedException(@NotNull final String cmd) {
        super("ERROR! Command: " + cmd + " not supported!");
    }

}
