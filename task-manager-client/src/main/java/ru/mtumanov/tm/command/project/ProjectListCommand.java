package ru.mtumanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.project.ProjectListRq;
import ru.mtumanov.tm.dto.response.project.ProjectListRs;
import ru.mtumanov.tm.enumerated.ProjectSort;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.Project;
import ru.mtumanov.tm.util.TerminalUtil;

import java.util.Arrays;

public class ProjectListCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Show list projects";
    }

    @Override
    @NotNull
    public String getName() {
        return "project-list";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(ProjectSort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final ProjectSort sort = ProjectSort.toSort(sortType);
        @NotNull final ProjectListRq request = new ProjectListRq(getToken(), sort);
        @NotNull final ProjectListRs response = getProjectEndpoint().projectList(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
            return;
        }
        int index = 0;
        for (final Project project : response.getProjects()) {
            if (project == null) continue;
            System.out.println(index + ". " + project);
            index++;
        }
    }

}
